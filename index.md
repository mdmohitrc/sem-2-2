# Semester :: 2:1

-----------------------------------------------------------------
## To do List ::

### Gsoc: [Gsoc MD](gsoc)
### MLH: [Application](./Me/bio/mlh.md)

* nvim add file name in lualine
* blind 75 leetcode list dp
* dsa pyq - library portal
* reapply mlh summer, MLH
* check last time su cab amount
* su cab last to last sem split amount proper? swd 
* cp course bits review look

* ps quiz @6-12 @lms/gform probably (grading to be done on it)
* It will be based on basics of the ps company, its products, what it works on, its competitors, its projects websites, etc
* Qs can be broad enough - go through other 2 companies too :: Americana group analytics (UAE Sharjah) :: Econnnect solutions udaipur

* What is merge sort and quick sort? which is better
* What is SVM? Types of SVM 
* All related to ES6 new features
* Asynchronous behaviour of Node js.
* What are event loops?
* How does set timeout work?
* Micro Tasks in NodeJs ?
* What is a B-tree?
* Problem of Edit distance.
* Discussion on data-warehousing.
* One simple probablity question.

### Academics
* [ ] Physical past year papers and resources             :: By :: <span style="color:#d65d0e;">28 july</span>
* [X] RESUME DOC LINK: https://docs.google.com/document/d/1pGFkmod1pKnWv22gB_4_cMvfiwm2Aq7c/
* [X] RESUME PDF DRIVE: https://drive.google.com/file/d/1uMdXTG6642lnF0ficbJI2Ik9nOKPAMtd/view?usp=sharing
* [ ] dsa lab3 ex1 doubt
* [ ] Lookup singhcoder github bookmark - 4 yrs notes

### Extra
* [X] Mail remote tab startups
* [X] Follow up on BITS-Review ADS course [Course-content](local:extra/cp-course_bits-review.pdf)
    - Seniors: Aakash Gupta (2018B4A70887P); Gautam Bhambhani (2019A7PS0101P); Kalash Shah (2018A7PS0213P) 
* [ ] Fix few bugs github portfolio
* [X] TA SHIP this sem?
* [ ] kpcb felloswship/ Nea next/ Bessemer fellowship/ 8 VC
* [ ] for summer? https://fellowship.hackillinois.org/
* [ ] [internfreak](https://internfreak.co/jobs-and-internship-opportunities?page=4&limit=15#article)
* [ ] D2C, etc view scholarships
* [ ] poe https://docs.google.com/spreadsheets/d/1bCu6l5ppxrzQk5hDf3qfABsTdB5GrDbSegF1U6NKUO0/edit#gid=0
* [ ] Google Hashcode

### Other
* [ ] Press to talk, gmeet
* [X] TAship approval

-----------------------------------------------------------------
## Schedule 
* TimeTable         : [TT Png](local:acads/tt.jpeg)
* Academic schedule : [Academic Pdf](local:acads/academic_tt.pdf)
* NOTE : Tommorow (19th jan) 5pm class on DSA is cancelled

## Midsem Schedule
* DBS  - 9th  March,  2 - 3:30  PM
%% * Acnc - 10th March,  2 - 3:30  PM
* DSA  - 11th March,  9 - 10:30 AM
* Ffin - 11th March,  4 - 5:30  PM
* Poe  - 12th March, 11 - 12:30 PM
* Evs  - 14th March,  9 - 10:30 AM
* MuP  - 15th March,  9 - 10:30 AM

## Courses
1. CS-F212   #4 [Database Management Systems](acads/Dbms/dbms)
2. CS-F211   #4 [Data Structures Algo](acads/Dsa/dsa)
3. CS-F241   #4 [Microprocessor Interfacing](acads/MuP/mup)
4. ECON-F211 #3 [Principle of Economics](acads/Poe/poe.md)
5. GS-F231   #3 [Asian Cinemas and Cultures](./acads/Acnc/acnc.md)
6. GS-F331   #3 [Fundamentals of Finance and Account](./acads/Ffin/ffin.md)
7. BITS-F225 #3 [Environmental Studies](acads/Evs/evs)

## Resources
* [Chirag Kakkar Drive](https://drive.google.com/drive/folders/1z5bJwXG8Ov7sLB_lDmSg7QH3Ss4zOv6Y?usp=sharing)
    - Course Drives
* [Previous Years Materials Drive](https://drive.google.com/drive/folders/1AgB447AOwWbN_n4_mYrnJyxdRaJHmK-z?usp=sharing)
    - Course Drives
* [SU 2nd Year Drive](https://drive.google.com/drive/folders/19AbpgczqHgNYU178Nv435zf4JJ-j_8fW?usp=sharing)
    - Course Drives
* [CS 2nd Year Drive](https://drive.google.com/drive/folders/1q7jxWl98gAcWE7QP6-bPdoFC_ejGGM8n?usp=sharing)
    - Course Drives
    - PS-1 Help Materials
    - Electives Guide / Huel Reviews
    
-----------------------------------------------------------------
## Other Wikis
* [[wiki0:index|Dotfiles Vimwiki]]
* [[wiki2:index|Experimental Vimwiki]]

-----------------------------------------------------------------
