/* 
Instructions
1.	First, execute the sample database file “india_motors.sql” to create a sample database.
2.	Perform the SQL query on that sample database.
3.	You must use the file “Answers_Template.sql” to paste SQL queries into a single file. After pasting all queries rename the SQL file with your complete BITS ID i.e., “2017PHXF0428P.sql”.
4.	Please paste your query in a specified place after a successful compilation. if you paste in the wrong place, zero marks will be given. 
5.	After completion of the exam, upload the SQL file on Nalanda. 

*/


/* 	Student Name :
	Student ID: 
    */
    
-- Q:1 ########### Paste SQL Query Below ##########################




-- Q:2  ########### Paste SQL Query Below ##########################
-- (a)


-- (b)

-- (c)



-- Q:3  ########### Paste SQL Query Below ##########################
-- (a)



-- (b)



-- (c)



-- (d)



-- (e)



-- Q:4  ########### Paste SQL Query Below ##########################



-- Q:5  ########### Paste SQL Query Below ##########################



-- Q:6 ########### Paste SQL Query Below ##########################
-- (a)


-- (b)


-- Q:7 ########### Paste SQL Query Below ##########################
-- (a)


-- (b)



-- Q:8  ########### Paste SQL Query Below ##########################