# DBMS

## Schedule
* Midsem: 
* Compre: 7 May : 9 AM

## Details
* Handout     : [Dbms-Handout](local:dbms-handout.pdf)
* Course no   : CS-F212
* Credits     : 4
* Course Total: 300 M
* Office Hours: Friday 5 - 6 PM @ 6111-C
* Lec Section : L1      :: M W F :: 9 - 10 AM :: Amit Dua / Yashwardhan Sharma :: [Meet Link](https://meet.google.com/mve-uuyw-pyp)
* Lab Section : P5      :: T     :: 8 - 10 AM :: Upendra Singh                 :: [Meet Link](https://meet.google.com/pat-njuk-ice)

## Grading
* 40% compre                          :: 120M ::
* 30% Midsem                          :: 90M  ::
* 10% Group Project(Group 4-5)        :: 30M  ::
* 10% Labtests(Final test)            :: 30M  :: 1st April?
* 5% Lab Attendance(Answer Qs in lab) :: 15M  ::
* 5% Quiz(Mcq/Short-q)                :: 15M  :: Feb 2nd week (18th?)
* HW - Regular non-eval class assignments 

## Assignments
1. [X] [File Handling Assignment](./ass/file-handling-ass) :: `Wed 26, 5PM`

## Labs
* [Setup](./labs/setup.md)
* see screenshot of quiz conducted in lab

-------------------------------------------------------------------------------------------
## Materials
* [Some McGraw material](http://www.mhhe.com/silberschatz)
* [Dbms-Tb](local:dbms-tb.pdf) [Official link](http://www.db-book.com)
* [Pearson R1](local:dbms-r1.pdf)  [Official link](http://www.aw.com/cssupport)
* [McGraw R2](local:dbms-r2.pdf) [Official link](http://www.cs.wisc.edu/dbbook)
* [Lec Recordings](https://drive.google.com/drive/u/1/folders/1hzdpYKkTXmK13v3CqMXrDGpkvNajYiqG)
* [Random Source](https://www.cs.carleton.edu/faculty/awb/cs334/s21/notes/)

## Resources
* [CS 2nd Year Drive](https://drive.google.com/drive/folders/1-4E58J-bdir3bqhBwJqgmULA8W8LjIRA?usp=sharing)
    - Old slides
    - Solved Labsheets
    - Solved Labtests
    - Midsem, Quiz Papers
* [Previous Year Drive](https://drive.google.com/drive/folders/1zcyuvLrVujSw0VHQr0cqMUbb4ZuDWM6Q?usp=sharing)
    - Midsem, Compre, Quiz Papers
* [SU Drive](https://drive.google.com/drive/folders/1_eL-t9nafhHTcgErSs3I0JxQO1zJwu8X?usp=sharing)
    - Chapterwise Practice Problems
    - Old Slides
    - Solved Labsheets
    - Solved Labtests
    - Midsem, Compre, Quiz Papers
