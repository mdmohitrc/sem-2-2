# File Handling Assignment - DBMS

## Problem:
* Write a file handling menu driven program in C programming Language.
* Display your postcom marks and grade of any course you have completed in last three semesters.
* You should also display the MGPA for each semester and overall CGPA.
* Make your program as user friendly as possble.

## Submission:
* You need to submit a zip file with all data files and c program files.
* By :: `Wed 26, 5PM`
