/* #include <cstddef> */
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

float CGPA;
int YEAR;
int SEMESTER;

// struct for courses undertaken
struct course{
    // name of the course
    char *name;
    // grade point awarded out of 10
    int grade_point;
    // letter grade awarded
    char grade;
    // year in which course was taken
    int year;
    // semester in which course was taken
    int semester;
};


// function acting as a constructor for course struct
struct course *createNewObject(char name[], int grade_point, char grade, int year, int semester){
    // assign memory for new object
    struct course *newObject = (struct course *)malloc(sizeof(struct course));
    // assign data
    newObject -> name = name;
    newObject -> grade_point = grade_point; 
    newObject -> grade = grade;
    newObject -> year = year;
    newObject -> semester = semester;

    // object created
    return newObject;
}


// split db line
char** str_split(char* a_str, const char a_delim){
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;
    /* Count how many elements will be extracted. */
    while (*tmp){
        if (a_delim == *tmp){
            count++;
            last_comma = tmp;
        }
        tmp++;
    }
    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);
    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;
    result = malloc(sizeof(char*) * count);
    if (result){
        size_t idx  = 0;
        char* token = strtok(a_str, delim);
        while (token){
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }
    return result;
}


// read from database file
void read_db(int metadata_read){
    // buffer string limit
    char buffer[9000];
    // reading csv file
    FILE* db_file = fopen("db.txt", "r");
    // check if header data is read
    // execute while buffer remains
    while(fgets(buffer, sizeof (buffer), db_file)){
        // data seperated via semicolons and commas in each data in csv file */
        char** tokens = str_split(buffer, ',');
        if(tokens){
            if(!metadata_read){
                printf("Final CG Data: ");
                for(int i = 0; *(tokens + i) && !metadata_read; i++){
                    printf(" %s ", *(tokens + i));
                    char** data = str_split(*(tokens + i), ':');
                    if(data){
                        for (int j = 0; *(data + j) && !metadata_read; j++){
                            /* printf("%s", *(data + j)); */
                            free(*(data + j));
                        }
                        metadata_read = 1;
                        free(data);
                    }
                    free(*(tokens + i));
                    return;
                }
                free(tokens);
            }
            else{
                for(int i = 0; *(tokens + i); i++){
                    printf(" %s ", *(tokens + i));
                    char** data = str_split(*(tokens + i), ':');
                    if(data){
                        for (int j = 0; *(data + j) && !metadata_read; j++){
                            /* printf("-data%d=[%s]-", j, *(data + j)); */
                            free(*(data + j));
                        }
                        free(data);
                    }
                    free(*(tokens + i));
                }
                free(tokens);
            }
        }
        /* char *data = strtok(buffer, ","); */
        /* if(!metadata_read){ */
        /*     while(data != NULL){ */
        /*         printf("Split %s; ", data); */
        /*         char* temp = data; */
        /*         data = strtok(NULL, ","); */
        /*         strtok(temp, ":"); CGPA = atof(strtok(temp, ":")); */
        /*         printf("HERE IS THE CGPA - %f\n", CGPA); */
        /*     } */
            /* strtok(data, ":"); CGPA = atof(strtok(NULL, ":")); */
            /* printf("HERE IS THE CGPA - %f\n", CGPA); */
            /* data = strtok(NULL, ","); strtok(data, ":"); YEAR = atoi(strtok(data, ":")); */
            /* printf("HERE IS THE Year - %d\n", YEAR); */
            /* metadata_read = 1; */
        /* } */
        /* while(metadata_read && data != NULL){ */
            /* if(!strcmp(data, "CGPA")){ */
            /*     CGPA = atof(strtok(NULL, ":")); */
            /* } */
            /* if(!strcmp(data, "YEAR")){ */
            /*     YEAR = atoi(strtok(NULL, ":")); */
            /* } */
            /* if(!strcmp(data, "SEMESTER")){ */
            /*     SEMESTER = atoi(strtok(NULL, ":")); */
            /* } */
            /* printf("Sep %s", data); */
            /* data = strtok(NULL, ":"); */
        /* } */
        /* break; */
        /* fclose(db_file); */
    }
}


// main function
int main(){
    int choice;
    printf("You can enter 0 to view final CG Data, or 1 to view the course grades data: \n");
    printf("Enter your choice[0/1]: \n");
    scanf("%d", &choice);
    /* printf("%d\n", choice); */
    if(choice){
        printf("Course Grades Data: ");
    }
    if(choice > 1 || choice < 0){
        printf("Invalid choice!");
    }
    else{
        read_db(choice);
    }
}
