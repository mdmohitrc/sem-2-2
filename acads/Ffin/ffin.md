# Fundamentals of Finance and Account

## Schedule
* Midsem: 
* Compre: 13 May : 3 PM

## Details
* Handout     : [Ffin-Handout](local:./ffin-handout.pdf)
* Course no   : ECON-F212
* Credits     : 3
* Course Total: 
* Office Hours: Tuesday & Thursday, 2:30 - 4:30 pm
* Lec Section : L2      :: M W F :: 4 - 5 PM :: Debata Byomakesh, Rajan Pandey (L1 & L1M) :: [Meet Link](https://meet.google.com/irf-zwxx-wso)

## Grading
* 40% Compre  :: 120M ::
* 30% Midsem  :: 90M  ::
* 30% Project :: 90M  ::


-------------------------------------------------------------------------------------------
## Materials
* Lec Recs - GCalendar
* [2nd Feb Recording](https://drive.google.com/file/d/11hgGNX8UmRIO3Y6wp7HnxFiWPnwYcAaN/view)
* [4th Feb Recording](https://drive.google.com/file/d/13l3eogEmWuG5U1kOmqTX0bWb4xYmOnPZ/view?usp=sharing)
* [L1 Recordings](https://drive.google.com/drive/folders/15eYX8sKvwCmyLAQ4LIrFhZflT47o4GuL)
* [Ffin-TB](local:tb.pdf)
* [Reference Book](local:r3.pdf)

## Resources
* [Chirag Kakkar Drive](https://drive.google.com/drive/u/1/folders/1cgLDIEkF80nRVglolw64GkF1B_SE6YeY)
    - Lecture slides and stuff
    - Few excel sheets
* [SU Drive](https://drive.google.com/drive/u/1/folders/1sXVZcXUTmFPj3B1r3TDOhVV2vpvknqMA)
    - Lecture slides ( + mit slides, dunno if useful)
    - Notes
* [Midsem-Compre](https://drive.google.com/drive/u/0/folders/1HIKYmp_hiSOnaZya3mO3B66-i8ktVx2m)
    - Midsem compre papers
    - few quizzes papers
* [Fundafin Drive](https://drive.google.com/drive/u/0/folders/1FhkxbyaEHFeqG6IgXXCzahsILJ256HTu)
    - Too many resources
* [Fundafin 2017-2019](https://drive.google.com/drive/u/0/folders/1jJmnLM3vXzdhtMPt9TOG1UPAtZnY19la)
    - Contained in above link
