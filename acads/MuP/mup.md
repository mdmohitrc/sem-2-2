# MUP

## Schedule
* Midsem: 
* Compre: 20 May : 9 AM

## Details
* Handout     : [MuP-Handout](local:mup-handout.pdf)
* Course no   : CS-F241
* Credits     : 4
* Course Total: 
* Office Hours: Saturday 12 - 1 PM
* Lec Section : L1 :: T Th :: 10 - 11 AM :: Vinay Chamola                    :: [Meet Link](https://meet.google.com/ogp-vrbi-scd)
* Lab Section : P5 :: T    :: 3 - 5 PM   :: Poonam Poonia / Sayyida Beegam K :: [Meet Link](http://meet.google.com/hfr-rqom-qng)
* Tut Section : T4 :: T    :: 5 - 6 PM   :: Vinay Chamola                    :: [Meet Link](https://meet.google.com/ogp-vrbi-scd)
* SLACK CHANNEL !

## Grading
* 40% Compre    ::  ::
* 30% Midsem    ::  ::
* 10% TutQuizes ::  :: ~5 TUT Tests[2M], 1M for attendance (Grace marks for answering)
* 10% LabAss    ::  :: Best 8/8-10 Labs[2M], 1M for attendance
* 10% Labtests  ::  :: TBA (After all labs)

## Labs


-------------------------------------------------------------------------------------------
## Materials
* [MuP-TB](local:mup-tb.pdf)
* [Study Companion](https://docs.google.com/spreadsheets/d/1i52OOR53Ahad50gB6viKA3v_dKXJZr8xO70ANu3NMig/edit#gid=0)

## Resources
* [CS 2nd Year Drive](https://drive.google.com/drive/u/1/folders/115Je9sQcy9J8cQL863FNbTaRm4ZJzFuL)
    - Proteus software, materials
    - Recordings (apart from course recs too)
    - Quiz, Midsem Papers
    - Labsheets, materials
* [Singh Coder Drive](https://drive.google.com/drive/u/1/folders/1-FIiXSXKZ-HD_D5rZKaWSNp_CfPGRxjo)
* [Previous Year Material Drive](https://drive.google.com/drive/u/1/folders/1XjuNBw6jx1wUHk2hoDKaSzfvE3SnCpAU)
    - Midsem, Compre, Quiz Papers
* [SU Drive](https://drive.google.com/drive/u/1/folders/1cByKcR7Ji3wH7N-UxvBmDyFcYGhGcqNd)
    - Midsem, Compre, Quiz, Tut Papers
    - Reference Sheets
    - Labsheets, materials
* [Mup Help Package](https://drive.google.com/drive/folders/1USaGXh4htgySUOi_a1HFpzEFkf3jGBJo)
