.model tiny
.386
.data
id db '2020A7PS0048'
count equ 4
subtractor equ 30h
offs equ 0048h
base equ 2020h
.code 
.startup
        lea si,id
        mov cl,count
x1:     mov al,[si]
        sub al,subtractor
        add bl,al
        inc si        
        dec cl
        jnz x1
        inc si
        mov al,[si]
        sub al,subtractor
        add bl,al
        inc si
        inc si
        inc si
        mov cl,count
x2:     mov al,[si]
        sub al,subtractor
        add bl,al
        inc si        
        dec cl
        jnz x2        
        mov ax,base
        mov ds,ax
        mov si,offs
        mov [si],bx
        .exit
        end
