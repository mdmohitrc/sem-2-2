.model tiny
.386

.data
year equ 2020h
id equ 0048h
len equ 19
NM db 'Mohit Dilip Makwana'
FLIP db 10 DUP(8)

.code 
.startup
    mov cx,19
    mov bx,2020h
    mov es,bx
    lea si,NM
    mov di,0048h
    add di,len
x1: lodsb
    mov es:[di],al
    dec di
    dec cx
    jnz x1
.exit
END
