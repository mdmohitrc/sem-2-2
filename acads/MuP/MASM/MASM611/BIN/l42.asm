.model tiny
.386
.data

len1     dw 000Eh
len2     dw 000Ah
inp1     db 14 dup("$")
inp2     db 10 dup("$")
username db "Mohit Makwana$"
password db "donkeyprom$"
star     db "*"
prt1     db "Enter User Name: ", 0Dh, 0Ah, "$"
prt2     db 0Dh, 0Ah, "Name entered by you: ", 0Dh, 0Ah, "$"
prt3     db 0Dh, 0Ah, "Name stored in db is: ", 0Dh, 0Ah, "$"
prt4     db 0Dh, 0Ah, "Name not matched, try again: ", 0Dh, 0Ah, "$"
prt5     db 0Dh, 0Ah, "Name matched, enter password: ", 0Dh, 0Ah, "$"
prt6     db 0Dh, 0Ah, "Password incorrect, try again: ", 0Dh, 0Ah, "$"
prt7     db 0Dh, 0Ah, "Hey there, Mohit Makwana!", 0Dh, 0Ah, "$"

.code
.startup

; Ask for input
pt1: LEA DX, prt1
     MOV AH, 09h
     INT 21h

; Take username inputs
inp: LEA DI, inp1
     MOV CX, len1
inpl: MOV AH, 01h
      INT 21h
      STOSB
      LOOP inpl

; Acknowledge user input
pt2: LEA DX, prt2
     MOV AH, 09h
     INT 21h
     LEA DX,inp1
     MOV AH,09h
     INT 21h

; Show stored names
pt3: LEA DX, prt3
     MOV AH, 09h
     INT 21h
     LEA DX, username
     MOV AH, 09h
     INT 21h

; Compare names
cpp: LEA SI, username
     LEA DI, inp1
     MOV CX, 0Eh
     REPE CMPSB
     JCXZ pt5
     JMP pt4

; If names dont match
pt4: LEA DX, prt4
     MOV AH, 09h
     INT 21h
     JMP inp

; If names match
pt5: LEA DX, prt5
     MOV AH, 09h
     INT 21h

; Take password inputs
inpp: LEA DI, inp2
      MOV CX, len2
inppl: MOV AH, 08h
       INT 21h
       STOSB
       MOV DL, star
       MOV AH, 02h
       INT 21h
       LOOP inppl

; Compare passwords
ppp: LEA SI, password
     LEA DI, inp2
     MOV CX, 0Ah
     REPE CMPSB
     JCXZ pt7
     JMP pt6

; If passwords dont match
pt6: LEA DX, prt6
     MOV AH, 09h
     INT 21h
     JMP inpp

; If passwords match
pt7: LEA DX, prt7
     MOV AH, 09h
     INT 21h

.exit
end
