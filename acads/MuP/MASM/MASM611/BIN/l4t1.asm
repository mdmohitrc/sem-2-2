.model tiny
.386
.data

max1 db 14
act1 db ?
inp1 db 14 dup(?)
outp db 0Dh, 0Ah, 14 dup(?), '$'
endr db '$'
; rev  db  14 dup(?), '$'

.code
.startup

;Take inputs
inp: LEA DX,max1
     MOV AH, 0Ah
     INT 21h

; fill in input between chars of output string
     MOV CL,max1
     LEA SI,inp1
     LEA DI,outp
     ADD DI,02h
fix: LODSB
     MOV [DI],AL
     INC DI
     LOOP fix

;Displaying the output
prt: LEA DX,outp
     MOV AH,09h
     INT 21h

.exit
end
