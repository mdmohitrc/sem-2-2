.model tiny
.386
.data

len      DB 05h
dat      DB 00h
row1     DB 0
spc1     DW 13*80
row2     DB 13
spc2     DW 13*80
space    DW 20h
; min_row  DW 6Ch
; max_row  DW 80
; curr_row DW 000Fh
; max_col  DW 25h
; min_col  DW 00h
; curr_col DW 25h
msg      DB "MOHIT"
rpt      DW 0001h

.code
.startup

; Set the display mode
setdp: MOV AH, 00h
       MOV AL, 03h ; set video mode
       INT 10h
       LEA SI, space

; Set the cursor position
setcp1: MOV AH, 02h
        MOV DH, row1 ; set row
        MOV DL, 00h ; set column
        MOV BH, 00h ; set page no
        INT 10h

; Write character at cursor
wrtch1: MOV AH, 09h
        MOV CX, spc1      ; set times to repeat char
        MOV AL, [SI]     ; set character
        MOV BH, 00h      ; set page no
        MOV BL, 1Eh      ; set attribute
        INT 10h

; Set the cursor position
setcp2: MOV AH, 02h
        MOV DH, row2 ; set row
        MOV DL, 00h ; set column
        MOV BH, 00h ; set page no
        INT 10h

; Write character at cursor
wrtch2: MOV AH, 09h
        MOV CX, spc2      ; set times to repeat char
        MOV AL, [SI]     ; set character
        MOV BH, 00h      ; set page no
        MOV BL, 7Ah      ; set attribute
        INT 10h

; Set the cursor position
setcp3: MOV AH, 02h
        MOV DH, row1 ; set row
        MOV DL, 00h ; set column
        MOV BH, 00h ; set page no
        INT 10h

; Fill color at pixel 
; fillc: MOV AH, 0Ch
;        LEA SI, min_col
;        MOV AL, 02h ; set pixel color
;        MOV CX, curr_col ; set column
;        MOV DX, curr_row ; set row
;        INT 10h
;        DEC BYTE PTR curr_col
;        MOV CX, curr_col
;        CMP CX, [SI]
;        JNZ fillc
;        LEA SI, max_col
; fillcrc: INC BYTE PTR curr_col
;          MOV CX, curr_col
;          CMP CX, [SI]
;          JNZ fillcrc
;        LEA SI, max_row
;        INC BYTE PTR curr_row
;        MOV CX, curr_row
;        CMP CX, [SI]
;        JNZ fillc

; Blocking function (! is the key)
       MOV AH, 07h
block: INT 21h
       ; CMP AL, '!' ; set blocking character
       ; JNZ block
type: MOV PREV, AL
      MOV BL, 1Eh
      MOV BH, 0
      MOV CX, 1
      MOV AH, 09h
      INT 10h
movec: MOV AH,02H
	   ADD DH,13
       INT 10H
      MOV AH, 09h
	  MOV BL, 7AH
  	  MOV CX,1
	  INT 10H

first: CMP AL, "#"
       JNZ type

       

.exit
end
