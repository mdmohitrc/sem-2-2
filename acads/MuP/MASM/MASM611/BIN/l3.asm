.model tiny
.386
.data

ID db '2020A7PS0048'
SET db 30h

.code
.startup

setup: MOV AX, 00h
       MOV CX, 04h
       LEA SI, ID

year: MOV AL, [SI]  
      SUB AL, SET
      ADD AH, AL
      INC SI
      LOOP year

rset: MOV CX, 04h
      ADD SI, 04h 

roll: MOV AL, [SI]
      SUB AL, SET
      ADD AH, AL
      INC SI
      LOOP roll

move: MOV DH, AH
      LEA SI, ID
      LODSW

.exit
end
