.model tiny
.386
.data

fname  DB "test", 0
handle DW ?
len    DW 05h
data1  DB "Mohit"
data2  DB "fifve"
inp    DB 5 dup("$")

.code
.startup

; Open a new file
; opn: MOV AH, 3Ch
;      LEA DX, fname
;      MOV CL, 2
;      INT 21h
;      MOV handle, AX

; Open existing file
ope: MOV AH, 3Dh
     MOV AL, 02h
     LEA DX, fname
     INT 21h
     MOV handle, AX
 
; Reading the file
red: MOV BX, handle ; move handle to BX
     MOV AH, 3Fh
     MOV CX, len
     LEA DX, inp
     INT 21h

; Reading till EOF
rede: MOV BX, handle ; move handle to BX
      LEA SI, inp
      MOV AH, 3Fh
      MOV CX, 01h
      LEA DX, inp
      INT 21h
      CMP 

; Moving file pointer to end
mfp: MOV AH, 42h
     MOV AL, 02h
     MOV BX, handle
     MOV CX, 0000h
     MOV DX, 0000h
     INT 21h
 
; Write to a file
wrt: MOV AH, 40h
     MOV BX, handle
     MOV CX, len
     LEA DX, inp
     INT 21h
 
; Close a file
cls: MOV AH, 3Eh
     MOV BX, handle
     INT 21h
 
.exit
end
