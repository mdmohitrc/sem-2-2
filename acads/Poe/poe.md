# POE

## Schedule
* Midsem: 
* Compre: 14 May : 3 PM

## Details
* Handout     : [Poe-Handout](local:poe-handout.pdf)
* Course no   : ECON-F211
* Credits     : 3
* Course Total: 
* Office Hours: Tuesday 4 PM @ 6165-M
* Lec Section : L1  :: M W F :: 10 - 11 AM :: Krishna / GeetiLaxmi / Rahul Arora / Balakrishna :: [Meet Link](https://meet.google.com/msa-nfjr-wdu)
* Tut Section : T10 :: Th    :: 2 - 3 PM   :: Angana Parashar Sarma                            :: [Meet Link](https://meet.google.com/dwo-fjww-eyb)

## Grading
* 40% Compre(Part OB)   ::  ::
* 30% Midsem(CB)        ::  ::
* 30% SURPRISE TEST(CB) ::  ::


-------------------------------------------------------------------------------------------
## Materials
* [Pearson Textbook](local:poe-tb.pdf)
* [McGraw R2](local:poe-r2.pdf)
* [Oxford R3](local:poe-r3.pdf)
* [Cengage R4](local:poe-r4.pdf)
* [Lec Recordings](https://drive.google.com/drive/u/1/folders/11HtM0UAJ7dwTVFG3KEH3bQ7liOBpSt0H)

## Resources
* [Chirag Kakkar Drive](https://drive.google.com/drive/u/1/folders/1CfUZd8c4mSbj0ZSgfCeBotpEBv8DHIYx)
    - Old slides
    - Midsem, Compre, Quiz, Tutorial papers
* [CS 2nd Year Drive](https://drive.google.com/drive/folders/1-1qkuysv8PT5__EYf9YGc5kkLBSU8Gxp?usp=sharing)
    - Tut tests
    - Midsem, Compre Papers
* [Previous Year Drive](https://drive.google.com/drive/folders/1-MPOsqvZpPr_bbSHr2ksbgvvIwLYQVsP?usp=sharing)
    - Midsem, Compre, Tut papers
* [SU Drive](https://drive.google.com/drive/folders/1koWuY6dZZG5JJ_vt1SDxcO-9XtYp8j9S?usp=sharing)
    - Same as Kakkar Drive
