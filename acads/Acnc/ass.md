# Comrades: Almost a love story

## Assignment 1:
In not less than 750 words offer an analysis of how the film Comrades, Almost a Love Story (Peter Chan, 1996)
engages with the anxieties around Hong Kong's imminent "return" to Mainland China. A link to the film is given below. 
You need to make sure that the write up is completely original. Marks will be deducted if your write ups are found to be plagiarised.
* Movie Link: https://drive.google.com/drive/u/0/folders/1NR5cJYmk3z5vR6RK9lLAxh3FHLdOfYpk


## Abstract:

## Content:
    
    Dear sir I have brokent ti into:1 2 3

    "Comrades: Almost a love story" is a film set in Hong Kong over the duration of many years, and portrays the general anxieties and loneliness 
among people during the fateful return of Hong Kong to China by the Britain during the mid 1990s. To provide context, the hero (Xiao-Jun) and 
heroine (Li Qiao) being native Norther and Cantonese Guangzhou, but both, essentially, Chinese Mainlanders migrate to Hong Kong in search of 
better opportunities. But in the loneliness and voidness of living in such a big city, they eventually fall in love with each other, but due to 
difference in ambitions, they get seperated. Xiao Jun marries his fiancee (Xiao Ting) after bringing her to Hong Kong, and Li Qiao marries a gang 
lord Pao and becomes rich. Being distressed due to their seperation, they eventually reunite at New York, after Xiao Jun arrives there as a 
cook (post confessing to his wife) and Li Qiao's husband being killed by muggers.

    Although, on the surface, the film appears simply to depict the love story of 2 people and the complications they face along the way before 
reuniting finally, there is much more to it. It beautifully captures the socio-political and cultural changes which Hong Kong was undergoing during
the 1990s and specifically during it's imminent handover to China. These "abstract" intentions can be observed and realized in many layers of meaning interpreted by 
the behaviour and mindset of people in many scenes of the movie. It initially shows the plight of immigrants from Mainland China, moving to 
Hong Kong in search for better opportunities and living with a better financial and social status. It also very delicately depicts the tradeoffs 
these people face, in terms of loneliness, and a void in the heart even after achieving their "Hong Kong Dream". It shows the complication and 
distortion of people's sense of patriotism with time and Hong Kongers' general aversion to immigrants, especially from Mainland China, even though 
most of them have descended from China.

    Hong Kong if often viewed as a portal by the chinese to achieve a better living in the best cities. However, it's official policies and even 
cultural attitude towards immigrants is not welcoming. This is in fact, portrayed accurately in the movie, as the ambitious Li Qiao always try to run 
away and separate herself from the Mainlandish identity and replace it by being a "fake Hong Konger". She shows in multiple ways throughout the movie,
like her pride in being able to communicate in English, and also speaking in Cantonese. She feels a deep satisfaction in this linguistic divide she 
created with her older identity, as she understands that although around 1/5th population of Hong Kong is immigrant mainlanders, not being able 
to speak Cantonese, far from English is what immediately gives one's identity away as a mainlander. Her aspiring and proud nature is reflected by her
constant pursuit of money making sources, involving in selling song CDs, buying stocks, running errands, working part-time at English classes, at McDonalds, 
etc. She bitterly detests her previous barren condition at the Mainland, and so much so that while running low on finances, when she joins as a Massager, she does 
it very unwillingly, as is depicted in one scene when Xiao Jun refers to her hands being sore in front of a receptionist of a jewellery store. Once 
when she revealed to Xiao Jun that she indeed is from Guangzhou, he refers to her as "fellow comrade" to which she argues strongly that they must not identify
themselves through their past in the Mainland. Instead they must feel more like "Hong Kongers" since they speak Cantonese, watch Hong Kong TV Shows, drink Vitasoy, etc. Even upon
achieving her entreprenual dreams much later, she rejoices in being separated from her native identity. We realize this by her conversation with Xiao Jun 
during their meet after being separated, where she talks happily about being unrecognizable at her home town, people directly speaking to her in English,
and not viewing her as a "Chinese girl" anymore. She again displays this tendency upon acquiring her green card, in her call to her father. She finally feels
she can dive again in her past as she is now a legal citizen of US and can live her dreams. (fake cover drop relax)

    The difficulties faced by the immigrants is depicted much more by Xiao Jun's struggle initially. The movie clearly depicts the isolation felt by immigrants 
in a city where they don't even share a common language with the people. It is so brutal that once Xiao Jun realizes that Li Qiao speaks Mandarian (when meeting
her for the first time at McDonalds), he pounces on the opportunity, and leans on her to guide him through surviving in the city. She teaches him to use ATM cards,
selling song CDs, enrolls him in an English class geared specifically towards immigrants. Xiao Jun remains grateful to her even though how Li Qiao uses him to 
get her work done very often, as he did not want to lose his only "friend". This shows how overwhelming surviving in Hong Kong is to immigrants. Even the well off 
trying to escape their "immigration status" is depicted in a scene where the two sell Teresa cassettes, but despite being an amazing singer, people resist buying
them, as it is generally the Mandarians who listen to her songs dearly. Apart from general non-acceptance, the immigrants also suffer cut-throat competition
to survive in the city among the mass of other immigrans. It is shown in a scene where Li Qiao feels no guilt in earning a commission off of enrolling Xiao Jun 
in English classes, which she justifies by referring him to a "Northerner" and thus not her "comrade" (She highlights this again by referring to her fluent Cantonese
being proof of her belonging to Hong Kong).

    Another aspect which is displayed beautifully in this movie is the hollowness left in the hearts of immigrants which remains unfilled even in the select
few who are able to successfully make a living in Hong Kong. After years of struggle and hustle, sacrificing all leisure and desires, working hard everyday,
being met with long sought financial and social status should be fulfilling. However, as the movie shows, nothing can replace the warmth of being with one's loved ones.
Going through all the struggles and hassles of Hong Kong initially was only possible for Xiao Jun and Li Qiao due to the support and motivation they recieved from 
each other. As is depicted later on in the movie, Li Qiao remains unhappy even after achieving her high dream suffering from multiple emotional blows like killing
of her husband, separation from her lover, the death of her mother before being able to witness her success (She had dreamt of buying multiple houses for herself and
her mom). Xiao Jun too loses his passion after separating from Xiao Ting for whom once he dreamt of bringing to Hong Kong and marrying. The story's end involves
emotionally charged scenes, as Xiao Jun and Li Qiao reunite on the day Teresa Tang dies, while watching the news of her death in front of a TV shop. This "blending"
of theirs back into their roots is what eventually satisfies them and fills the void in their hearts. It turns out, ironically, that Xiao Jun and Li Qiao had been
seated back to each other on the train en-route to Hong Kong. On a quest to fulfil their ambitious goals, they did not realize that, the only source of their happiness had been
right along with them from the start, after all.
