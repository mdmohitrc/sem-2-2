# Asian Cinemas and Cultures

## Schedule
* Midsem: 
* Compre: 10 May : 9 AM

## Details
* Handout     : [Acnc-Handout](local:acnc-handout.pdf)
* Course no   : HSS-F368
* Credits     : 3
* Course Total: 
* Office Hours: Tuesdays, 4 - 5 pm
* Lec Section : L1      :: M W F :: 11 - 12 PM :: Muhammad Afzal P :: [Meet Link](https://meet.google.com/guv-yrty-zvz)

## Grading
* 40% Compre         ::  ::
* 30% Midsem         ::  ::
* 30% Paper/Ass/Quiz ::  ::

## Midsem
* Oldboy - Korean
%% * Ayat Ayat Chinta - Indonesian
* City of Sadness - Taiwan
* Shanghai Dreams - Chinese
* Akira - Japan
* In the mood for love - Hong Kong
* Comrades - Hong Kong
 
 singapore dreaming left points:
 ambitions: mie and husband visiting houses they cant afford
 morals: mie treating maid badly, believing her to have  stolen money, further taunts her husband.
 send and mei arguing even at funeral, seng foolishly deciding to use will's money anywhere
 
-------------------------------------------------------------------------------------------
## Materials
* [Movies Drive](https://drive.google.com/drive/u/1/folders/1R4y-WzlFlEARCTXYd_L8z2bd-EhqlNNJ)
* [Lec Recordings](https://drive.google.com/drive/u/1/folders/1nIMKbdaHtaqHv7EuPj4wj-ds3D6KQrDq)
* GClassroom

## Resources
* [Last Year Materials](https://drive.google.com/drive/folders/1hFQu0xrDUFn2lU-AUXt714GQOtY-xAGu)
