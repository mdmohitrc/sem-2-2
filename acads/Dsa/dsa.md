# DSA

## Schedule
* Midsem: 
* Compre: 12 May : 9 AM

## Details
* Handout     : [Dsa-Handout](local:dsa-handout.pdf)
* Course no   : CS-F211
* Credits     : 4
* Course Total: 300 M
* Office Hours: 
* Lec Section : L1      :: T Th; W :: 11 - 12 AM; W : 5 - 6 PM :: Jagat Sesh / Vishal Gupta :: [Meet Link](https://meet.google.com/eeg-rkeh-fva)
* Lab Section : P6      :: Th      :: 3 - 6 PM                 :: Syed Rizvi                :: [Meet Link](https://meet.google.com/iig-mjqp-kmj)

## Grading
* 43% Compre :: 129M ::
* 30% Midsem :: 90M  ::
* 20% Quiz   :: 60M  ::
* 7% Labs    :: 21M  :: Best 7/8 eval labs[3M] (total 10-14), submission within 3 days of lab (SUNDAY)

## Labs - [Home](./labs/labs.md)
1. [Lab1](local:lab1.pdf)
2. [Lab2](local:lab2.pdf)
3. [Lab3](local:labs/lab3.pdf)


-------------------------------------------------------------------------------------------
## Materials
* [Lecture Recordings](https://drive.google.com/drive/u/1/folders/1w6kPM9E_Jpv5eS5ocbbE3Te21SVG42I9)
* [Labs Recordings](https://drive.google.com/drive/u/1/folders/1TnMyt8kYT9p9ZznrsPHds3fXmy_7ZBpX)
* [Dsa-TB](local:dsa-tb.pdf)
* [Cormann MIT Press R1](local:dsa-r1.pdf)
* [Cormann R1 Sol](local:dsa-r1-sol.pdf)

## Resources
* [CS 2nd Year Drive](https://drive.google.com/drive/u/1/folders/1-OSOWT904R_th-2yxrVDznjxIu2JojTP)
    - Midsem grade report
    - Old slides
    - Midsem, Compre, Quiz, Assignment Papers
    - Labtests, Lab source codes
* [BITS Hyd Labsheets](https://drive.google.com/drive/folders/1TF50QqfygsVLlLNZXQP61o-xgu_IAESD)
    - Labsheets (might be in c++)
* [Previous Year Drive](https://drive.google.com/drive/u/1/folders/1XjuNBw6jx1wUHk2hoDKaSzfvE3SnCpAU)
    - Midsem, Compre Papers
* [SU Drive](https://drive.google.com/drive/u/1/folders/1YcsULmSny3-W2JJTa8DRsWVHh_WIZsAu)
    - Old slides
    - Midsem, Compre, Quiz, Assignment Papers
    - Labtests, Lab source codes
