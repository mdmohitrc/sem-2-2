#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "linked_list.h"

/** IMPLEMENTATIONS ---------------------------------------------- */

Node * createLinkedList(int num){
    Node * head = (Node *)malloc(sizeof(Node));
    head -> data = 1;
    head -> next = NULL;
    while(--num){
        int random = rand() % 200;
        head = addNodeFirst(head, random);
    }
    return head;
}

int getListSize(Node * head){
    if(head == NULL){
        return 0;
    }
    int length = 1;
	Node * temp = head;
	while(temp != NULL){
		temp = temp -> next;
        length++;
	}
    return length;
}

void displayList(Node * head){
	Node * temp = head;
    int count = 0;
	while(temp != NULL && count < 50){
		printf("%d\n", temp -> data);
		temp = temp -> next;
        count++;
	}
}

Node * getNode(Node * head, int position){
    if(position >= getListSize(head)){
        return head;
    }
    Node * tempNode = head;
    while(position--){
        tempNode = tempNode -> next;
    }
    return tempNode;
}

Node * addNodeFirst(Node * head, int element){
	Node * newNode = (Node *)malloc(sizeof(Node));
	newNode -> data = element;
	newNode -> next = head;
    head = newNode;
    return head;
}

Node * addNodeLast(Node * head, int element){
    Node * newNode = (Node *)malloc(sizeof(Node));
    newNode -> data = element;
    newNode -> next = NULL;
    Node * posNode = getNode(head, getListSize(head) - 1);
    posNode -> next = newNode;
    return head;
}

Node * addNode(Node * head, int element, int position){
    if(position >= getListSize(head)){
        return head;
    }
    Node * posNode = getNode(head, position);
    Node * newNode = (Node *)malloc(sizeof(Node));
    newNode -> data = element;
    newNode -> next = posNode -> next;
    posNode -> next = newNode;
    return head;
}

Node * deleteNodeFirst(Node * head){
    Node * temp = head;
    head = head -> next;
    free(temp);
    return head;
}

Node * deleteNodeLast(Node * head){
    Node * tempNode = getNode(head, getListSize(head) - 2);
    Node * posNode = tempNode -> next;
    tempNode -> next = NULL;
    free(posNode);
    return head;
}

Node * deleteNode(Node * head, int position){
    Node * tempNode = getNode(head, position - 1);
    Node * posNode = tempNode -> next;
    tempNode -> next = tempNode -> next -> next;
    free(posNode);
    return tempNode;
}
