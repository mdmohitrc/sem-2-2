#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/** Structure for 1 Node */
struct node{
	int data;
	struct node * next;
};
/** Alias for node structure */
typedef struct node Node;

/** 
Create new random linked list of length num 
** @returns - head
*/
Node * createLinkedList(int num);

/**
Gets size of linked list 
** @returns - size of list
*/
int getListSize(Node * head);

/** Print contents of linked list */
void displayList(Node * head);

/** 
Gets node pointing to a position in list 
** @returns - pointer to node at position if exists, else head
*/
Node * getNode(Node * head, int position);

/** 
Add node to start of linked list 
** @returns - pointer to first node
*/
Node * addNodeFirst(Node * head, int element);

/** 
Add node to end of linked list 
** @returns - pointer to last node
*/
Node * addNodeLast(Node * head, int element);

/** 
Add node to a position  of linked list 
** @returns - pointer to node at position + 1 if applicable, else head
*/
Node * addNode(Node * head, int element, int position);

/** 
Delete first node 
** @returns - head
*/
Node * deleteNodeFirst(Node * head);

/**
Delete first node 
** @returns - head
*/
Node * deleteNodeLast(Node * head);

/** 
Delete node at a position 
** @returns - pointer to node at position - 1 if applicable, else head
*/
Node * deleteNode(Node * head, int position);
