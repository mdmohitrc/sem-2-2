#include "header.h"
#include <stdio.h>

int main(int argc, char ** argv){
    FILE *fp_in; FILE *fp_out;
    if (argc >= 2){ fp_in = fopen(argv[1], "r"); }
    else{ fp_in = fopen("file.txt", "r"); }
	if(fp_in == NULL){ printf("Error opening file\n"); }

	employee * arr = (employee *)malloc(2000*sizeof(employee));
	int i = 0;
	while(!feof(fp_in)){
		fscanf(fp_in, "%s %lld", arr[i].name, &arr[i].empid);
		i++;
	}
	fclose(fp_in);

    if (argc >= 3){ fp_out = fopen(argv[2], "w"); }
    else{ fp_out = fopen("out.txt", "w"); }
	if(fp_in == NULL){ printf("Error opening file\n"); }

	int size = i;
    int cutoff_size = estimateCutoff(arr, size);
    double time_elapsed = ideal_sort(arr, size, cutoff_size);
	time t = testrun(arr,size);
    printList(arr,size);

    if(fp_out){
        fprintf(fp_out, "Insertion sort time = %lf  Quick sort time = %lf\n",t.ist,t.qst);
        fprintf(fp_out, "Ideal sort time = %lf \n\n",time_elapsed);
        fprintf(fp_out, "Estimated Cutoff size: %d\n\n",cutoff_size);
        printListFile(arr,size, fp_out);
    }
    else{
        printf("Insertion sort time = %lf  Quick sort time = %lf\n",t.ist,t.qst);
        printf("Ideal sort time = %lf \n\n",time_elapsed);
        printf("Estimated Cutoff size: %d\n\n",cutoff_size);
        printList(arr,size);
    }
    fclose(fp_out);
	return 0;
}
