#include "header.h"


void swap(employee* array,int a,int b){
    employee temp;
    temp=array[a];
    array[a]=array[b];
    array[b]=temp;
    return;
}


int partition(employee arr[], int l, int h){
	int size = h - l +1;
	int pivot = l +(rand()%size);
	swap(arr,h,pivot);
	int i = l-1;
	int j;
	for(j = l; j <= h-1; j++){
		if(arr[j].empid <= arr[h].empid){
			i++;
			swap(arr,i,j);
		}
	}
	swap(arr,i+1,h);
	return i+1;
}

void quickSort(employee arr[], int l, int h, int cutoff){
	stack * st = NULL;
	st = push(st,l,h);
	if(h - l <= 0)
		return;
	while(!isEmpty(st)){
		stack s = top(st);
		int low = s.low;
		int high = s.high;
		st = pop(st);
		int p = partition(arr,low,high);
		if(cutoff>1){
			if(p>(low+cutoff)){
				st=push(st,low,p-1);
			}
			if((p+cutoff)<high){
				st=push(st,p+1,high);
			}
		}
		else{
			if(p>(low+1)){
				st=push(st,low,p-1);
			}
			if((p+1)<high){
				st=push(st,p+1,high);
			}
		}
	}
}


void insertionSort(employee arr[], int size){
	int i,j;
	for(i = 0; i < size; i++){
		employee key = arr[i];
		j = i-1;
		while(j >= 0 && arr[j].empid > key.empid){
		 	arr[j+1] = arr[j];
		 	j--;
		 }
		arr[j+1] = key;
	}
}

double ideal_sort(employee arr[], int size, int cutoff_size){
    time t = testrun(arr, size);
    if(size >= cutoff_size){
        return t.qst;
    }
    else{
        return t.ist;
    }
}
