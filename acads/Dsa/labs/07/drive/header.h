#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>

//typedef long long ll;

struct Employee
{
	char name[11];
	long long int empid;
};

typedef struct Employee employee;

struct Stack
{
	int low;
	int high;
	struct Stack * next;
};


typedef struct Stack stack;

struct times
{
	double ist,qst;
};

typedef struct times time;


stack* push(stack * s, int l, int h);
bool isEmpty(stack* top);
stack * pop(stack * s);
stack top(stack * s);
void printList(employee* array,int size);
void printListFile(employee* array,int size, FILE *fp);
void insertionSort(employee arr[], int size);
void quickSort(employee arr[], int l, int h, int cutoff);
double ideal_sort(employee arr[], int size, int cutoff_size);
int partition(employee arr[], int l, int h);
void swap(employee* array,int a,int b);
int estimateCutoff(employee arr[],int size);
time testrun(employee arr[],int size);



