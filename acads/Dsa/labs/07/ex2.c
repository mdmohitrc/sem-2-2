#include "header.h"

time testrun(employee arr[],int size){
	employee arr1[size],arr2[size];
	for(int i = 0; i<size; i++){
		arr1[i] = arr[i];
		arr2[i] = arr[i];
	}

	struct timeval t1,t2;
	double elapsedTime;
	gettimeofday(&t1,NULL);
	insertionSort(arr1,size);
	gettimeofday(&t2,NULL);
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
	elapsedTime += (t2.tv_usec - t1.tv_usec)/1000.0 ;		      

	struct timeval t3,t4;
	double elapsedTime1;
	gettimeofday(&t3,NULL);
	quickSort(arr2,0,size-1,0);
	gettimeofday(&t4,NULL);
	elapsedTime1 = (t4.tv_sec - t3.tv_sec) * 1000.0;
	elapsedTime1 += (t4.tv_usec - t3.tv_usec)/1000.0 ;

	time t;
	t.ist = elapsedTime;
	t.qst = elapsedTime1;

	return t;
}

int estimateCutoff(employee arr[],int size){
	time t1 = testrun(arr,2);
	time t2 = testrun(arr,size);

	time t;
	int min = 2, max = size, mid;

	do{
		mid = (min+max)/2;
		t = testrun(arr,mid);
		if(t.qst<t.ist)
			max = mid;
		else if(abs(t.qst-t.ist)<0.0001)
			break;
		else
			min = mid;
	}while(1);

	return mid;
}
