#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int baseNumber,tableSize;
struct read
{
	char ** book;
	int size;
};

typedef struct read Book;

struct node
{
	int index;
	int count;
	char * str;
	struct node * next;
};

typedef struct node Node;

struct hash
{
	int elementCount;
	int insertionCost;
	int queryingCost;
	Node ** arrLink;
};

typedef struct hash hashtable;

int hashFunction(char * str)
{
	int i,hash,sum = 0;
	for(i = 0; i < strlen(str); i++)
	{
		sum += str[i];
	}
	hash = sum % baseNumber;
	return (hash%tableSize);
}

int collisionCount(char ** arr,int size)
{
	int i,index,count = 0;
	int temp[tableSize];
	for(i = 0; i < tableSize; i++)
	{
		temp[i] = -1;
	}
	for(i = 0; i < size; i++)
	{
		index = hashFunction(arr[i]);
		if(temp[index] == -1)
			temp[index] = 1;
		else
			count ++;
	}
	return count;
}


Book* parsing(FILE * fp)
{
	char temp[100000][15];
	char str;
	int z,x;
	char c;
	x=0;
	int count;
	while(1)
	{
		z = 0;
		count = 0;
		c = fgetc(fp);
		if(feof(fp))
			break;
		while((c>='a' && c<='z') || (c>='A' && c<='Z'))
		{
			count = 1;
			temp[x][z] = c;
			z++;
			c = fgetc(fp);
			if(feof(fp))
				break;
		}
		if(count == 1)
		{
			temp[x][z] = '\0';
			x++;
		}
		if(feof(fp))
			break;
	}

	Book* new = (Book *)malloc(sizeof(Book));
	new -> book = (char **)malloc(x*sizeof(char *));
	for(int j = 0; j < x; j++)
	{
		new->book[j] = (char *)malloc((strlen(temp[j])+1)*sizeof(char));
		strcpy(new->book[j],temp[j]);
	}
	new -> size = x;
	return new;
}

void profiling(char ** book, int size)
{
	int baseNumber1[3][6];
	int tableSize1[3];
	tableSize1[0] = 5000;
	tableSize1[1] = 50000;
	tableSize1[2] = 500000;
	int baseindexmin;
	int tableindexmin;
	int count;
	int countmin = 1000000;
	int basenumin,tablemin;
	for(int i = 0; i < 3; i++)
	{
		int basecount = 0;
		for(int z = tableSize1[i]; z < 2*tableSize1[i];z++)
		{
			int flag = 0;
			for(int j = 2; j < z/2; j++)
			{
				if(z%j == 0)
				{
					flag = 1;
					break;
				}
			}
			if(flag == 0)
			{
				baseNumber1[i][basecount] = z;
				basecount++;
			}
			if(basecount >= 3)
				break;
		}
		for(int z = 1000*tableSize1[i];;z++)
		{
			int flag = 0;
			for(int j = 2; j < z/2; j++)
			{
				if(z%j == 0)
				{
					flag = 1;
					break;
				}
			}
			if(flag == 0)
			{
				baseNumber1[i][basecount] = z;
				basecount++;
			}
			if(basecount >= 6)
				break;
		}
	}
	int i = 0;
	while(i < 3)
	{
		tableSize = tableSize1[i];
		for(int z = 0; z < 6; z++)
		{
			baseNumber = baseNumber1[i][z];
			count = collisionCount(book,size);

			if(count < countmin)
			{
				countmin = count;
				basenumin = baseNumber1[i][z];
				tablemin = tableSize1[i];
				baseindexmin = z+1;
				tableindexmin = i+1;
			}
		}
		i++;
	}
	baseNumber = basenumin;
	tableSize = tablemin;
	printf("best choice of basenumber is %d index is %d and for tableSize is %d index is %d which gives count %d \n",basenumin,baseindexmin,tablemin,tableindexmin,countmin);
}

hashtable* creation()
{
	hashtable* ht = (hashtable *)malloc(sizeof(hashtable));
	ht -> elementCount = 0;
	ht -> insertionCost = 0;
	ht -> queryingCost = 0;
	ht -> arrLink = (Node **)malloc(tableSize*sizeof(Node*));
	for(int i = 0; i < tableSize; i++)
	{
		ht -> arrLink[i] = NULL;
	}
	return ht;
}


void insert(hashtable * ht, char ** A,int j)
{
	if(ht == NULL)
	{
		printf("Hashtable Invalid\n");
		return;
	}
	int index = hashFunction(A[j]);
	Node * temp = ht -> arrLink[index];
	if(temp == NULL)
	{
		Node * new = (Node *)malloc(sizeof(Node));
		new -> count = 1;
		new -> index = j;
		new -> str = (char *)malloc((strlen(A[j])+1)*sizeof(char));
		strcpy(new->str,A[j]);
		new -> next = NULL;
		ht -> arrLink[index] = new;
		ht -> elementCount ++;
		return;
	}
	while(temp -> next != NULL && strcmp(temp -> str,A[j])!=0)
	{
		temp = temp -> next;
		ht -> insertionCost++;
	}
	if(strcmp(temp->str,A[j])==0)
		temp->count ++;
	else
	{
		Node * new = (Node *)malloc(sizeof(Node));
		new -> count = 1;
		new -> index = j;
		new -> str = (char *)malloc((strlen(A[j])+1)*sizeof(char));
		strcpy(new->str,A[j]);
		new -> next = NULL;
		temp -> next = new;
		ht -> elementCount++;
	}

}

int insertAll(hashtable * ht, char ** book,int size)
{
	for(int i = 0; i < size; i++)
	{
		insert(ht,book,i);
	}
	return ht -> insertionCost;
}

Node * lookUp(hashtable * ht, char * str)
{
	if(ht == NULL)
	{
		printf("Hashtable invalid\n");
		return NULL;
	}

	int index = hashFunction(str);
	Node * temp = ht -> arrLink[index]; 
	if(temp == NULL)
	{
		printf("%s not in Hashtable\n",str);
		ht->queryingCost++;
		return NULL;
	}
	while(temp -> next != NULL && strcmp(temp->str,str)!=0)
	{
		temp = temp->next;
		ht -> queryingCost++;
	}
	if(strcmp(temp->str,str) == 0)
	{
		ht -> queryingCost++;
		return temp;
	}
	else
		return NULL;
}

int lookUpAll(hashtable * ht,char ** A,int size,float m)
{
	int entries = m* size;
	int z;
	for(int i = 0 ; i < entries; i++)
	{
		z = i % size;
		Node * temp = lookUp(ht,A[z]);
	}
	return ht->queryingCost;
}
int main()
{
	FILE * fp = fopen("aliceinwonderland.txt","r");
	if(fp == NULL)
	{
		printf("File doesn't exist\n");
		return 0;
	}
	Book * book1 = parsing(fp);
	printf("Parsing successful\n");
	baseNumber = 5011;
	tableSize = 5000;

	hashtable * ht = creation();
    
	printf("Insertion cost = %d\n",insertAll(ht,book1->book,book1->size));
	char ** temp = book1 -> book;
	for(int i = 0; i < book1->size; i++)
	{
		//printf("%s\n",temp[i] );
	}
    printf("There are %d valid strings.\n",book1->size); 
	printf("Query cost = %d\n",lookUpAll(ht,book1->book,book1->size,0.1));
	
	return 0;
}