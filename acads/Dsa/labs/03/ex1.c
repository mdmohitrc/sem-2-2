#include <stdio.h>
#include <stdlib.h>

int bits;
void p(int p);
void g();
void h();
void d();

void main() {	
	int pilani;
	float pilani2;
	p(pilani);
    p(pilani2);
    p(343143331);
	g();
    h();
    d();
    int trial = 10;
    printf("\nTRIAL SIZE: %u  TRIAL VALUE: %d\n", sizeof(trial), trial);
}

void p(int pilani) {
	printf("GLOBAL: %u  PILANI :%u\n", &bits, &pilani);
	/* p(pilani+1); */
}

void g() {
	int goa;
	printf("GLOBAL: %u  GOA    :%u\n", &bits, &goa);
	/* h(); */
}

void h() {
	int hyd;
	printf("GLOBAL: %u  HYD    :%u\n", &bits, &hyd);
    /* d(); */
}

void d() {
	int dub;
	printf("GLOBAL: %u  DUBAI  :%u\n", &bits, &dub);
}
