#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "linkedlist.h"


bool testCyclic(Node * head){
	if(head == NULL || head->next == NULL){
		return 0;
    }
	Node * currentNode = head;
	Node * nextNode;
	Node * previousNode = NULL;
	while(currentNode){
		nextNode = currentNode->next;
		currentNode->next = previousNode;
		previousNode = currentNode;
		currentNode = nextNode;
	}
	if(previousNode == head){
		return 1;
    }
	else{
		return 0;
    }
}
