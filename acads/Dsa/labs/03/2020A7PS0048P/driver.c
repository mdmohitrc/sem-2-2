#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "linkedlist.h"

size_t totalSpace = 0;

int main(){
    /* for(int i = 5; i < 16; i++){ */
        Node * head = createList(15000000);
        head = createCycle(head);
        // Calculate the time taken by testCyclic
        clock_t t;
        t = clock();
        bool result = testCyclic(head);
        t = clock() - t;
        double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
        if(result){
            printf("Is a cycle\n");
        }
        else{
            printf("Is not a cycle\n");
        }
        printf("Final total heap space: %zu\n", totalSpace);
        printf("Execution time of testCyclic is: %f\n", time_taken);
    /* } */
}
