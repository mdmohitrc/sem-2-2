#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "linkedlist.h"

Node * addNodeFirst(int element, Node * head){
	Node * newNode = (Node *)myalloc(sizeof(Node));
	newNode -> data = element;
	newNode -> next = head;
    head = newNode;
    return head;
}

void displayList(Node * head){
	Node * temp = head;
    int count = 0;
	while(temp != NULL && count < 50){
		printf("%d\n", temp -> data);
		temp = temp -> next;
        count++;
	}
}

Node * createList(int num){
    Node * head = (Node *)myalloc(sizeof(Node));
    head -> data = 1;
    head -> next = NULL;
    while(--num){
        int random = rand() % 200;
        head = addNodeFirst(random, head);
    }
    return head;
}

Node * createCycle(Node * head){
    // no point working on linked lists of size less than 2
    srand(time(NULL));
    if(head -> next == NULL || head == NULL){
        return head;
    }
    // decide whether to create a cycle
    int chance = rand() % 2;
    if(chance == 0){
        printf("Not creating a cycle. . .\n");
        return head;
    }
    // create a cycle
    else{
        int length = 1;
        Node * last = head;
        while(last -> next != NULL){
            length++;
            last = last -> next;
        }
        printf("Linked list length: %d\n", length);
        int cycle_start = rand() % length;
        printf("Chosen index to start cycle at: %d\n", cycle_start);
        Node * cycle_node = head;
        while(cycle_start--){
            cycle_node = cycle_node -> next;
        }
        last -> next = cycle_node;
        return head;
    }
}

void * myalloc(size_t req_size){
    totalSpace += req_size;
    void * ptr = malloc(req_size);
    return ptr;
}

void myfree(Node * ptr){
    totalSpace -= sizeof(*ptr);
    printf("Size of pointer to free: %zu\n", sizeof(*ptr));
    printf("Total space: %zu\n", totalSpace);
    free(ptr);
}

Node * deleteNode(Node * head){
    Node * temp = head;
    head = head -> next;
    myfree(temp);
    return head;
}
