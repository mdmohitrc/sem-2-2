#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// Structure for 1 Node
struct node{
	int data;
	struct node * next;
};
typedef struct node Node;

// Total memory allocated
extern size_t totalSpace;

// Add node to linked list
Node * addNodeFirst(int element, Node * head);

// Print contents of linked list
void displayList(Node * head);

// delete first node (used it for testing)
Node * deleteNode(Node * head);

// Faster moving pointer - 2 nodes/move, Slower moving pointer - 1 node/move  || Reverse links in linked list
bool testCyclic(Node * head);

// Create new linked list of length num (exercise 3)
Node * createList(int num);

// Create a cyclic linked list based on a probability (exercise 3)
Node * createCycle(Node * head);

// custom allocation function (exercise 2)
void * myalloc(size_t req_size);

// custom deallocation function (exercise 2)
void myfree(Node * p);
