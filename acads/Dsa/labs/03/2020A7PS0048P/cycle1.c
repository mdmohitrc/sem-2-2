#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include "linkedlist.h"

bool testCyclic(Node * head){
    Node *slow = head;
    Node *fast = head;
     
    while(slow && fast && fast -> next){
        slow = slow -> next;
        fast = fast -> next -> next;
        if(slow == fast){
            return 1;
        }
    }
    return 0;
}
