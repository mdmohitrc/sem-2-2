/* driver.c */
#include <stdio.h>
#include "linkedlist.h"

int main(int argc, char *argv[]){
    FILE * fileptr = fopen(argv[1], "r");
    struct linkedList * head = (struct linkedList *) malloc (sizeof(struct linkedList));
    while (!feof(fileptr)){
        // read the next element and store into the temp variable.
        int temp;
        fscanf(fileptr, "%d ", &temp);
        // insert temp into the linked list.
        insertFirst(head,temp);
    }
    // close the file pointer
    fclose(fileptr);
    // print the linked list.
    printList(head);
    // delete the first element of the linked list.
    // print the linked list again to check if delete was successful.
    // print the linked list to a new file.
}
