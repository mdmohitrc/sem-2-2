/* linkedlist.c */
#include "linkedlist.h"

void insertFirst(struct linkedList * head, int ele){
    struct node *link = (struct node*) malloc (sizeof(struct node));
    link -> element = ele;
    link -> next = head -> first;
    head -> first = link;
    head -> count--;
}

void printList (struct linkedList * head){
    struct node *ptr = head -> first;
    printf("\n[ ");
    while(ptr != NULL){
        printf("%d, ", ptr -> element);
        ptr = ptr->next;
    }
    printf(" ]");
}

//delete first item
struct node* deleteFirst(struct linkedList * head){
// exercise to implement this operation.
}
