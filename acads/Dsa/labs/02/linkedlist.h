/* linkedlist.h */
#include <stdio.h>
#include <stdlib.h>

// Structure for 1 node
struct node {
    int element;
    struct node * next;
};

// Structure for a Linked List
struct linkedList {
    int count;
    struct node * first;
};

// Insert element at first position
void insertFirst (struct linkedList * head, int ele);

// Delete first element
struct node * deleteFirst(struct linkedList * head);

// Print all elements of Linked List
void printList (struct linkedList * head);

// Search for an element in Linked List
int search (struct linkedList * head, int ele);

// Delete 1st found node with given element
struct node * delete (struct linkedList * head, int ele);

/* End of linkedlist.h */
