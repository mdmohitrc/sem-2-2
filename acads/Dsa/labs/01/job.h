/***********file:   job.h *********/

// MAX - maximum no of jobs on list at a time
#define MAX 10

// enum of lowest to highest type of priorities
typedef enum {PRI_0, PRI_1, PRI_2} Priority;
typedef int Job_Id;
typedef int ExecutionTime;
typedef int ArrivalTime;

/* Structure for Jobs
**  id: Job id
**  pri: Job Priority
**  et: Execution time
**  at: Arrival time
*/
typedef struct{
    Job_Id id;
    Priority pri;
    ExecutionTime et;
    ArrivalTime at;
} Job;

// Array of all jobs
typedef Job JobList[MAX];
