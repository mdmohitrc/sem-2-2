#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "que.h"


Queue * newQ(){
    Queue * createdQueue = (Queue *)malloc(sizeof(Queue));
    createdQueue -> head = NULL;
    createdQueue -> tail = NULL;
    return createdQueue;
}

bool isEmptyQ(Queue * q){
    return lengthQ(q) == 0;
}

void printQ(Queue * q){
    Node * temp = q -> head;
    while(temp != NULL){
        printf("%d\n", temp -> data);
        temp = temp -> next;
    }
}

int lengthQ(Queue * q){
    if(q -> tail == NULL){
        return 0;
    }
    int length = 1;
    Node * temp = q -> head;
    while(temp -> next != NULL){
        length++;
        temp = temp -> next;
    }
    return length;
}

Element front(Queue * q){
    return q -> head ? q -> head -> data : 0;
}

Queue * addQ(Queue * q, Element e){
    if(lengthQ(q) == 0){
        Node * temp = (Node *)malloc(sizeof(Node));
        temp -> data = e;
        temp -> next = NULL;
        q -> head = q -> tail = temp;
        return q;
    }
    Node * temp = (Node *)malloc(sizeof(Node));
    temp -> data = e;
    temp -> next = NULL;
    q -> tail -> next = temp;
    q -> tail = temp;
    return q;
}

Queue * delQ(Queue * q){
    if(lengthQ(q) == 0){
        return q;
    }
    Node * temp = q -> head;
    q -> head = q -> head -> next;
    if(q -> head == NULL){
        q -> tail = NULL;
    }
    free(temp);
    return q;
}
