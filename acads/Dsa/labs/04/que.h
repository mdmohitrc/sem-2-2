/** Definitions of all functions used to implement an ADT: queue (FIFO) using linked list */

/** Queue will store Element type of int */
typedef int Element;

/** Structure of a node */
struct node{
    Element data;
    struct node * next;
};
/** Alias for node structure */
typedef struct node Node;

/** Structure of a queue (stores both front and rear node) */
struct queue{
    Node * head;
    Node * tail;
};
/** Alias for queue structure */ 
typedef struct queue Queue;

/** 
Create an empty Queue
** @returns - Queue pointer to a node
*/
Queue * newQ();

/** Checks if Queue is empty */
bool isEmptyQ(Queue * q);

/** Prints contents of Queue */ 
void printQ(Queue * q);

/**
Get length of Queue
** @returns - Length(int) of given queue
*/ 
int lengthQ(Queue * q);

/**
Gets the element at front of the queue
** @returns - First queue element
*/
Element front(Queue * q);

/**
Adds Element to rear of the queue
** @returns - Modified Queue's head
*/
Queue * addQ(Queue * q, Element e);

/** 
Deletes an element from the front
** @returns - Modified Queue's head
*/ 
Queue * delQ(Queue * q);
