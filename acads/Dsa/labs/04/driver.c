#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "que.h"

int main(){
    Queue * q = newQ();
    printf("Length is: %d\n", lengthQ(q));
    printf("Is empty : %d\n", isEmptyQ(q));
    printf("Front is: %d\n", front(q));
    printf("Printing queue . . .\n");
    printQ(q);
    addQ(q, 2);
    printf("Length is: %d\n", lengthQ(q));
    printf("Is empty : %d\n", isEmptyQ(q));
    printf("Front is: %d\n", front(q));
    printf("Printing queue . . .\n");
    printQ(q);
    addQ(q, 4);
    printf("Length is: %d\n", lengthQ(q));
    printf("Is empty : %d\n", isEmptyQ(q));
    printf("Front is: %d\n", front(q));
    printf("Printing queue . . .\n");
    printQ(q);
    delQ(q);
    printf("Length is: %d\n", lengthQ(q));
    printf("Is empty : %d\n", isEmptyQ(q));
    printf("Front is: %d\n", front(q));
    printf("Printing queue . . .\n");
    printQ(q);
    delQ(q);
    printf("Length is: %d\n", lengthQ(q));
    printf("Is empty : %d\n", isEmptyQ(q));
    printf("Front is: %d\n", front(q));
    printf("Printing queue . . .\n");
    printQ(q);
}
