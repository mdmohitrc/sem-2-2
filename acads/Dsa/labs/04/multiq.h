#include <stdio.h>
#include "que.h"

typedef Queue * MultiQ;

MultiQ createMQ(int num);
	
MultiQ addMQ(MultiQ mq, struct task t);

struct task nextMQ(MultiQ mq);

MultiQ delNextMQ(MultiQ mq);

int isEmptyMQ(MultiQ mq);

int sizeMQ(MultiQ mq);

int sizeMQbyPriority(MultiQ mq, int p);

Queue getQueueFromMQ(MultiQ mq, int p);
