#include <stdio.h>
#include "queue.h"

typedef struct queue * MultiQ;
MultiQ createMQ(int num);
MultiQ addMQ(MultiQ mq, struct task t);
struct task nextMQ(MultiQ mq);
MultiQ delNextMQ(MultiQ mq);
int isEmptyMQ(MultiQ mq);
int sizeMQ(MultiQ mq);
int sizeMQbyPriority(MultiQ mq, int p);
struct queue getQueueFromMQ(MultiQ mq, int p);
