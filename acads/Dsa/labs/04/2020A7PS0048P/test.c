#include <stdio.h>
#include "queue.h"

int main(){
	struct queue q;
	q = newQ();
	// printf("%d", isEmpty(q));
	delQ(q);
	struct task *t1 = (struct task *) malloc(sizeof(struct task));
	struct task *t2 = (struct task *) malloc(sizeof(struct task));
	struct task *t3 = (struct task *) malloc(sizeof(struct task));
	
	t1->id = 123;
	t1->p = 0;
	t2->id = 124;
	t2->p = 1;
	t3->id = 324;
	t3->p = 2;
	
	q = add(q,*t1);
	q = add(q,*t2);
	q = add(q,*t3);
	printf(" %d\n", lengthQ(q));

	q = delQ(q);
	printf(" %d\n", lengthQ(q));
	q = delQ(q);
	printf(" %d\n", lengthQ(q));
	q = delQ(q);
	printf(" %d\n", lengthQ(q));
	
	return 0;
}
	
	
