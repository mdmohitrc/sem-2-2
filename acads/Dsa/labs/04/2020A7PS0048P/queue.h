#include <stdio.h>
#include <stdlib.h>

struct task{
	int id;
	int p;
};
struct node{
	struct task data;
	struct node *next;
};
struct queue{
	struct node *head;
	struct node *tail;
};

struct queue newQ();

int isEmpty (struct queue q);

struct queue delQ (struct queue q);

struct task front (struct queue q);

struct queue add (struct queue q, struct task ele);

int lengthQ (struct queue q);
	
