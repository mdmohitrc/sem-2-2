#include <stdio.h>
#include "queue.h"

struct queue newQ(){
	struct queue *ptr = (struct queue *) malloc(sizeof(struct queue));
	ptr->head = NULL;
	ptr->tail = NULL;
	return * ptr;
}

int isEmpty (struct queue q){
	if(q.head == NULL)
	return 1;
	
	else
	return 0;
}

struct queue delQ (struct queue q){
	if(isEmpty(q)){
		printf("Queue is empty can't delete");
	}
	else{
		struct node *temp =  q.head;
		q.head = (q.head)->next;
		free(temp);
	}
	return q;
}

struct task front (struct queue q){
	if(!(isEmpty(q)))
	return (q.head)->data;
	else
	printf("no data in queue");
}

struct queue add (struct queue q, struct task ele){
	struct node * ptr = (struct node *) malloc(sizeof(struct node));
	ptr->next = NULL;
	ptr->data = ele;
	if(isEmpty(q)){
		q.head = ptr;
		q.tail = ptr;
	}
	else{
		(q.tail)->next= ptr;
		(q.tail) = ptr;
	}
	return q;
}

int lengthQ (struct queue q){
	struct node * temp = q.head;
	int count = 0;
	while(temp != NULL){
		count++;
		//printf("\n %d",temp->data);
		temp = temp->next;
	}
	return count;
}
