#include <stdio.h>
#include "multiq.h"

MultiQ createMQ(int num){
	MultiQ m = (MultiQ) malloc(num*sizeof(struct queue));
	for(int i=0;i<num;i++){
		m[i] = newQ();
	}
	return m;
}
	
MultiQ addMQ(MultiQ mq, struct task t){
	int temp = t.p-1;
	mq[temp] = add(mq[temp],t);
	return mq;
}
	

struct task nextMQ(MultiQ mq){
	for(int i=4;i>=0;i--){
		if(!isEmpty(mq[i])){
			return front(mq[i]);
		}
	}
}

MultiQ delNextMQ(MultiQ mq){
	for(int i=9;i>=0;i--){
		if(!isEmpty(mq[i])){
			struct queue q = mq[i];
			q = delQ(q);
			mq[i] = q;
			return mq;
		}
	}	
}

int isEmptyMQ(MultiQ mq){
	int flag=1;
	for(int i=0;i< 10;i++){
		if(!(isEmpty(mq[i]))){
			flag = 0;
		}
	}
	return flag;
}

int sizeMQ(MultiQ mq){
	int sum =0;
	for(int i=0;i <= 10;i++){
		sum = sum+ lengthQ(mq[i]);
	}
	return sum;
}

int sizeMQbyPriority(MultiQ mq, int p){
	return lengthQ(mq[p]);
}

struct queue getQueueFromMQ(MultiQ mq, int p){
	return mq[p];
}
