#include "header.h"

Node * newNode(int val){
	Node * temp = (Node *)malloc(sizeof(Node));
	temp -> data = val;
	temp -> left = NULL;
	temp -> right = NULL;
	temp -> height = 0;
	return temp;
}

Node* add(Node * t,int val){
	if(t == NULL)
		return newNode(val);
	if(val < t->data)
		t -> left = insert(t->left,val);
	else if(val > t->data)
		t -> right = insert(t->right,val);

	return t;
}

Node * search(Node * root,int key){
	if(root == NULL || root->data == key)
		return root;
	if(root->data < key)
		return search(root->right,key);

	return search(root->left,key);

}

Node * minValueNode(Node * node){
	Node * current = node;
	while(current -> left != NULL)
	{
		current = current -> left;
	}
	return current;
}

Node * deleteNode(Node * root,int key){
	if(root == NULL)
		return root;
	if(key < root->data)
		root->left = deleteNode(root->left,key);
	else if(key > root->data)
		root->right = deleteNode(root->right,key);
	//Here program comes if key is same as root's key
	else
	{
		if(root->left == NULL)
		{
			Node * temp = root->right;
			free(root);
			return temp;
		}
		else if(root->right == NULL)
		{
			Node * temp = root->left;
			free(root);
			return temp;	
		}

		Node * temp = minValueNode(root->right);

		root ->data = temp->data;
		root->right = deleteNode(root->right,temp->data);

	}
}


Node * rotate(Node * t, Node * x,Node* y,Node * z,Node* parent){
	if(t == NULL)
		return t;
	else{
		if(z -> left == y && y -> left == x){
			Node* t1 = z -> right;
			Node* t2 = y -> right;

			if(parent != NULL && z == parent->left)
				parent->left = y;
			else if(parent != NULL && z == parent->right)
				parent-right = y;
			else
				t = y;
			y -> right = z;
			z -> left = t2;
		}
		else if(z -> right == y && y -> right == x){
			Node* t1 = z -> left;
			Node* t2 = y -> left;

			if(parent != NULL && z == parent->left)
				parent -> left = y;
			else if(parent != NULL && z == parent->right)
				parent->right = y;
			else
				t = y;
			y -> left = z;
			z -> right = t2;
		}
		else if(z -> left == y && y -> right == x){
			Node* t1 = z->right;
			Node* t2 = y->left;
			Node* t3 = x -> left;
			NOde* t4 = x -> right;

			if(parent != NULL && z == parent->left)
				parent -> left = x;
			else if(parent != NULL && z == parent->right)
				parent->right = x;
			else
				t = x;

			x -> left = y;
			x -> right = z;
			y -> right = t3;
			z -> left = t4; 
		}
		else  //z -> right == y && y -> left == x
		{
			Node* t1 = z -> left;
			NOde* t2 = y -> right;
			Node* t3 = x -> left;
			Node* t4 = x -> right;

			if(parent != NULL && z == parent->left)
				parent -> left = x;
			else if(parent != NULL && z == parent->right)
				parent->right = x;
			else
				t = x;

			x -> left = z;
			x-> right = y;
			z -> right = t3;
			y -> left = t4;
		}

		return t;

	}
}

