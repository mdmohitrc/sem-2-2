#include <stdio.h>
#include <stdlib.h>

struct node{
	long long int card_num;
	char bank_code[5];
	char exp_date[7];
	char fname[10];
	char lname[10];
};

typedef struct node Node;

void pretty_print(Node * arr, int n);
void insertInOrder(Node * arr, int n, Node data);
void insertionSort(Node * arr, int n);
