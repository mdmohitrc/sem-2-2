#include<stdio.h>
#include <stdlib.h>
#include "record.h"


void pretty_print(Node * arr, int n){
	for(int i=0; i<n; i++){
		long long int t = arr[i].card_num;
		int count = 0;
		while(t != 0){
			t = t/10;
			count++;
		}
		if(count == 16){
			printf("%lld",arr[i].card_num );
        }
		else if(count == 15){
			printf("%lld ",arr[i].card_num );
        }
		else if(count == 14){
			printf("%lld  ",arr[i].card_num );
        }
		else if(count == 13){
			printf("%lld   ",arr[i].card_num );
        }
		printf("\t");
	
		for(int j = 0; j < (sizeof(arr[i].bank_code) / sizeof(char)); j++){
			printf("%c",arr[i].bank_code[j]);
		}
		printf("\t");
		for(int j = 0; j < (sizeof(arr[i].exp_date) / sizeof(char)); j++){
			printf("%c",arr[i].exp_date[j]);
		}
		printf("\t");
		for(int j = 0; j < (sizeof(arr[i].fname) / sizeof(char)); j++){
			printf("%c",arr[i].fname[j]);
		}
		printf("\t");
		for(int j=0; j < (sizeof(arr[i].fname) / sizeof(char)); j++){
			printf("%c",arr[i].lname[j]);
		}
		printf("\n");
	}
}


void insertInOrder(Node * arr, int n, Node data){
	int i = n - 1;
	while(i >= 0 && arr[i].card_num > data.card_num){
		arr[i + 1] = arr[i];
		i--;
	}
	arr[i + 1] = data;
}

void insertionSort(Node * arr, int n){
	if(n <= 1){
		return;
    }
	insertionSort(arr, n - 1);
	insertInOrder(arr, n - 1, arr[n - 1]);
}
