#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "record.h"

int main(){	
	struct timeval time_1,time_2;
	double value_time;
	gettimeofday(&time_1, NULL);
	int size = 10;
	Node * arr = (Node *)malloc(size*sizeof(Node));
	FILE *file_pointer = fopen("100000", "r");

	char temp;
	int i = 0;
	long long int t;

	while(fscanf(file_pointer, "%c%lld,%[^,],%[^,],%[^,],%[^\"]\"\n", &temp, &t, arr[i].bank_code, arr[i].exp_date, arr[i].fname, arr[i].lname) != EOF){
		arr[i].card_num = t;
		i++;
		if(i == size){
			size = size * 10;
			arr = (Node *)realloc(arr,size*sizeof(Node));
		}
	}
	fclose(file_pointer);
	pretty_print(arr, 100000);

	insertionSort(arr, 100000);
	printf("Sorted\n");
	pretty_print(arr, 100000);

	gettimeofday(&time_2,NULL);

	value_time = (time_2.tv_sec - time_1.tv_sec)*1000.0;
	value_time += (time_2.tv_usec - time_1.tv_usec)/1000.0;

	printf("Total time taken is %f ms\n",value_time);
	return 0;
}
