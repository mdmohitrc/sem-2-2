# EVS

## Schedule
* Midsem: 
* Compre: 18 May : 9 AM

## Details
* Handout     : [Evs-Handout](local:evs-handout.pdf)
* Course no   : BITS-F225
* Credits     : 3
* Lec Section : L1        :: M W F :: 6 - 7 PM :: Rita Sharma / Muthukumar / Kuncharam / Bhanu Vardhan Reddy :: [Meet Link](https://meet.google.com/ekz-jstr-whf )

## Materials
* [Evs-TB](local:evs-tb.pdf)
* [Evs-TB2](local:evs-tb2.pdf)
* Reference books in SU drive

## Resources
* [Chirag Kakkar Drive](https://drive.google.com/drive/u/1/folders/1DdMuri3f0G9iNB2vJyGNcER_tvAs6xui)
    - Slide materials
* [SU Drive](https://drive.google.com/drive/u/1/folders/1od6iL5bPNyZXX1sNkv0Gmc5HFgNWjZPG)
    - Reference Books
    - Lecture Slides, extra materials
