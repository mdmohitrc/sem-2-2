from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.gridlayout import GridLayout
from kivy.lang import Builder
from kivy.animation import Animation
from kivy.properties import NumericProperty
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

import math 

# NOTE: The .kv file (dial_theme.kv) will setup the "frontend" components for the project
# It gets picked up automatically, since we have named it as a prefix of our Widge class + .kv

class Dial(Widget):
    angle = NumericProperty(0)

    def on_touch_down(self, touch):
        y = (touch.y - self.center[1])
        x = (touch.x - self.center[0])
        calc = math.degrees(math.atan2(y, x))
        self.prev_angle = calc if calc > 0 else 360+calc
        self.prev_angle -= 90
        print(self.prev_angle)
        self.tmp = self.angle

    def on_touch_move(self, touch):
        y = (touch.y - self.center[1])
        x = (touch.x - self.center[0])
        calc = math.degrees(math.atan2(y, x))
        new_angle = calc if calc > 0 else 360+calc

        self.angle = self.tmp + (new_angle-self.prev_angle)%360

    def on_touch_up(self, touch):
        print(f"self.prev_angle {self.prev_angle} self.angle {self.angle}")
        Animation(angle=self.prev_angle,d=0).start(self)


class DialApp(App):
    def build(self):
        return Dial()

if __name__ == "__main__":
    DialApp().run()
