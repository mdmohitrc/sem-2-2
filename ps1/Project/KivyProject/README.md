# Dial Project
## *Author*: **Mohit Makwana**
## *BITS ID*: **2020A7PS0048P**

## Features:
* This Project utilitizes the kivy framework to build a response dial. (Reference: https://kivy.org/doc/stable/gettingstarted/installation.html)

* Kivy is used to quickly and easily generate python applications with a clean GUI too.

* Here we store the "frontend" components or the UI of the project in a .kv file.

* The file is to be named as the Base Class of our Widget Class (which gets extended) and with a `.kv` extension.

* The file automatically gets picked up by Kivy when we run the main python file to generate the required UI components for the project


## Steps to Run:
* There needs to be a few dependancies to be installed for running this project:
    1. Kivy 


### Installation Steps:

#### Create virtual environment:
* Create a new virtual environment for the Kivy project. A virtual environment will prevent possible installation conflicts with other Python versions and packages.

* We can create a virtualenv named kivy by:
    `python -m virtualenv kivy_venv`

* To Activate the virtual environment, we will have to do this step from the current directory every time we start a new terminal. This sets up the environment so the new "kivy" Python is used.

* For Windows default CMD, in the command line do:
    `kivy_venv\Scripts\activate`

* If we are in a bash terminal on Windows, instead do:
    `source kivy_venv/Scripts/activate`

* If we are in linux or macOS, instead do:
    `source kivy_venv/bin/activate`


#### Install Kivy:
* Finally, install Kivy using one of the following options:

* The simplest is to install the current stable version of kivy and optionally kivy_examples from the kivy-team provided PyPi wheels. Simply do:
`python -m pip install "kivy[base]" kivy_examples`


#### Run Project:
* After installing dependancies; running the project is a simple step:
    `python main_kivy.py`


#### Final Test:
* After running, a GUI with a dial should appear on the screen;
* Notice that on clicking anywhere on the screen, the dial would move to that angle.
* This is not quite what the original project demanded, (entering angle in a textbox and making the dial move); but it tries to emulate that functionality to the closest extent possible.
