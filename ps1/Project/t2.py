from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.gridlayout import GridLayout
from kivy.lang import Builder
from kivy.animation import Animation
from kivy.properties import NumericProperty
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

import math 

kv = '''
<Dial>:
    canvas:
        Rotate:
            angle: root.angle
            origin: self.center
        Color:
            rgb: 1, 1, 0
        Ellipse:    
            size: min(self.size), min(self.size)
            pos: 0.5*self.size[0] - 0.5*min(self.size), 0.5*self.size[1] - 0.5*min(self.size)
        Color:
            rgb: 0, 0, 0
        Ellipse:    
            size: 50, 50
            pos: 0.5*root.size[0]-25, 0.9*root.size[1]-25
<textinp>:
    title: 'InputDialog'
    auto_dismiss: False
    id: test1
  
    # Using relative layout to arrange properly
    RelativeLayout:
        orientation: 'horizontal'
        pos: self.pos
        size: 30, 40
        id: test2
  
        # Defining text input in .kv
        # And giving it the look . pos and features
        TextInput:
            id: input
            hint_text:'Enter text'
            pos_hint: {'center_x': 0.5, 'center_y': 0.705}
            size_hint: 0.95, 0.5
            on_text: app.process()
'''
Builder.load_string(kv)

class Dial(Widget):
    angle = NumericProperty(0)

    def on_touch_down(self, touch):
        y = (touch.y - self.center[1])
        x = (touch.x - self.center[0])
        calc = math.degrees(math.atan2(y, x))
        self.prev_angle = calc if calc > 0 else 360+calc
        self.prev_angle -= 90
        print(self.prev_angle)
        self.tmp = self.angle

    def on_touch_move(self, touch):
        y = (touch.y - self.center[1])
        x = (touch.x - self.center[0])
        calc = math.degrees(math.atan2(y, x))
        new_angle = calc if calc > 0 else 360+calc

        self.angle = self.tmp + (new_angle-self.prev_angle)%360

    def on_touch_up(self, touch):
        print(f"self.prev_angle {self.prev_angle} self.angle {self.angle}")
        Animation(angle=self.prev_angle,d=0).start(self)

# Create the widget class
class textinp(Widget):
    pass

class DialApp(App):
    def build(self):
        # return Dial()
        parent = Widget()
        parent.add_widget(Dial())
        parent.add_widget(textinp())
        return parent

if __name__ == "__main__":
    DialApp().run()
