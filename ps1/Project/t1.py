#!/usr/bin/kivy
import kivy
kivy.require('1.7.2')
import math

from random import random
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.gridlayout import GridLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.graphics import Color, Ellipse, Rectangle

class MinimalApp(App):
    title = 'My App'
    def build(self):
        root = RootLayout()
        return(root)

class RootLayout(AnchorLayout):
    pass

class Circley(RelativeLayout):
    angle = 0
    def on_touch_down(self, touch):
        ud = touch.ud
        ud['group'] = g = str(touch.uid)
        return True
    def on_touch_move(self, touch):
        ud = touch.ud
#        print(touch.x, 0)
#        print(self.center)
#        print(0, touch.y)
#        print(touch.x - self.center[0], touch.y - self.center[1])
        y = (touch.y - self.center[1])
        x = (touch.x - self.center[0])
        calc = math.degrees(math.atan2(y,x))
        angle = calc if calc > 0 else (360 + calc)
        print(angle)
    def on_touch_up(self, touch):
        touch.ungrab(self)
        ud = touch.ud
        return True

if __name__ == '__main__':
    MinimalApp().run()
