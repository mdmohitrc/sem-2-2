.MODEL TINY
.DATA
	attrb   	DB	04H ; pixel color
	attrb2   	DB	06H ; pixel color
	rowBg   	DW	100
    rowMd       DW  225
    rowCr       DW  100
	rowEn		DW	350
	colBg   	DW	310
	colCr		DW	1
	origdisp	DB	?
	
.CODE
.STARTUP	

; Store old display and edit display mode
disp: MOV AH, 0FH
	  INT 10H
	  MOV origdisp, AL ; save original video mode
	  MOV AL, 12H      ; set graphical video mode
	  MOV AH, 0
	  INT 10H
	
; fill pixels
fill: MOV AH, 0Ch 
      MOV AL, attrb
      MOV CX, colBg
      ADD CX, colCr
      MOV DX, rowCr
	  INT 10h
      SUB CX, colCr
      MOV AL, attrb2
      SUB CX, colCr
      INT 10h
      DEC colCr
      CMP colCr, -01h
      JNZ fill
      ; increase row
      INC rowCr
      MOV CX, rowMd
      CMP rowCr, CX
      JGE colDec
      ; reset col current
colInc: MOV CX, rowCr
        MOV colCr, CX
        MOV CX, rowBg
        SUB colCr, CX
        JMP rowChk
colDec: MOV CX, rowEn
        MOV colCr, CX
        MOV CX, rowCr
        SUB colCr, CX
      ; check if last row reached
rowChk: MOV CX, rowEn
        CMP rowCr, CX
        JNZ fill
	
; Blocking logic code
block: MOV AH, 07H
	   INT 21H	
	   CMP AL, "!"      ; "!" is the key to exit
	   JNZ block
       MOV AL, origdisp ; restore the original video mode
       MOV AH, 00h
	   INT 10H
	
.EXIT
END
