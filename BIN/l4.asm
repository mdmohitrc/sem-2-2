.model tiny
.386
.data
max1 db 14
act1 db ?
inp1 db 14 dup(?)
rev db  14 dup(?), '$'
.code
.startup

;Take inputs
LEA DX,max1
MOV AH, 0Ah
INT 21h

;Store in reverse order in the memory of rev
mov cl, act1
lea si, inp1
lea di, rev

x1: inc si
    loop x1

mov cl, act1
x2: mov al, [si]
    mov [di], al
    dec si
    inc di
    loop x2
 
;Displaying the output
LEA DX, rev
MOV AH, 09h
INT 21h
.exit
end
