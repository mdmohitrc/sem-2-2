.model tiny
.data

match1 db 0h,10h,20h,30h,40h,50h,'$'
match2 db 0h,10h,20h,30h,40h,50h,'$'
match3 db 0h,10h,20h,30h,40h,50h,'$'
match4 db 0h,10h,20h,30h,40h,50h,'$'
total dw 0h,0h,0h,0h,0h,0h,'$'

.code
.startup

    mov si, 0
    lea bx, match1

ad: mov al,[bx][si]
    cmp al,'$'
    je endit
    call TOT
    inc si
    jmp ad

endit:
    .exit

TOT PROC NEAR USES BX DI AX DX
mov ax, 0
mov dx, 0
lea bx, match1
mov di, si
mov dl, [bx][si]
add ax, dx
lea bx, match2
mov dl, [bx][si]
add ax, dx
lea bx, match3
mov dl, [bx][si]
add ax, dx
lea bx, match4
mov dl, [bx][si]
add ax, dx
lea bx, total
shl di, 1
mov [bx][di], ax
RET
TOT ENDP

end
