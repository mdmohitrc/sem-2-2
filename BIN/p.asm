.model tiny

.data

ARRAY1 db 45h,54h,0f4h,56h,99h,0f9h,0f0h,87h,66h,23h,54h,0f3h,0f6h,9ch,0feh
ARRAY2 db 2 dup(?)

.code
.startup

    LEA SI, ARRAY1
    MOV BX, [SI]
    MOV ARRAY2, BL
    LEA DI, ARRAY2

.exit 
end
