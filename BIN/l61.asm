.model tiny
.386
.data

len DB 05h
row DB 0Ch
col DB 25h
msg DB "MOHIT"
rpt DW 0001h

.code
.startup

; Set the display mode
setdp: MOV AH, 00h
       MOV AL, 03h ; set video mode
       INT 10h
       LEA SI, msg

; Set the cursor position
setcp: MOV AH, 02h
       MOV DH, row ; set row
       MOV DL, col ; set column
       MOV BH, 00h ; set page no
       INT 10h

; Write character at cursor
wrtch: MOV AH, 09h
       MOV CX, rpt      ; set times to repeat char
       MOV AL, [SI]     ; set character
       MOV BH, 00h      ; set page no
       MOV BL, 8Fh      ; set attribute
       INT 10h
       INC BYTE PTR col ; move cursor column
       INC SI           ; string position
       DEC BYTE PTR len ; count of chars printed
       JNZ setcp
 
; Blocking function (! is the key)
       MOV AH, 07h
block: INT 21h
       CMP AL, '!' ; set blocking character
       JNZ block

.exit
end
